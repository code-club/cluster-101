#!/bin/bash

# Job name
#SBATCH --job-name=my_incredible_job

# Partition to use
#SBATCH --partition all
##SBATCH --exclusive

# Resources
#SBATCH --nodes 1          # Number of nodes
#SBATCH --mem 32Gb         # Memory
#SBATCH --time 00:30:00    # Time

# Specify a work directory
#SBATCH --chdir=/home/lmaurin/cluster-101

echo "Starting at `date`"
echo "Running on hosts: $SLURM_NODELIST"
echo "Running on $SLURM_NNODES nodes."
echo "Running on $SLURM_NPROCS processors."
echo "Task per node $SLURM_TASKS_PER_NODES"
echo "job cpu per node = $SLURM_JOB_CPUS_PER_NODE"
echo "Current working directory is `pwd`"

source .venv/bin/activate


python3 my_script.py
