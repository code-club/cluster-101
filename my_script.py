import numpy as np
import matplotlib.pyplot as plt
import time

x = np.linspace(0,10,100)

for i in range(10):
    y = np.random.randn(x.size)

    plt.figure()
    plt.plot(x, y)
    plt.savefig(f'test_{i}.png')
    time.sleep(30)
